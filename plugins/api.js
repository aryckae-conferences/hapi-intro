"use strict";

const api = {
    name: "api",
    register: async (server, options) => {

        server.route({
            method: "GET",
            path: "/data",
            handler: async (request, h) => {
                return {
                    data: ['a', 'b', 'c']
                };
            }
        });
    }
}

module.exports = api;
