'use strict'

const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const init = async () => {
    const server = Hapi.server({
        host: 'localhost',
        port: '3031'
    });

    await server.register(require('./plugins/api'), {
        routes: {
            prefix: '/api'
        }
    });

    server.route({
        method: 'GET',
        path: '/{name?}',
        handler: async (request, h) => {
            const name = request.params.name;
            return (name) ? `Hello ${name}` : 'Hello World';
        },
        options: {
            validate: {
                params: Joi.object({
                    'name': Joi.string().min(3).max(10).optional()
                })
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/sum/{number*}',
        handler: async (request, h) => {
            let numbers = request.params.number.split('/');
            try {
                Joi.assert(numbers, Joi.array().items(Joi.number()));
                numbers = numbers.map(n => parseInt(n));
            }
            catch (err) {
                throw new Boom.badRequest('Not all parameters are numbers');
            }

            const sum = numbers.reduce((a,b) => a+b);

            return {
                numbers: numbers,
                sum: sum
            }
        }
    })

    await server.start();
    console.log('Server listening on: %s', server.info.uri);
}

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
